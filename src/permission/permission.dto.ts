export type PermissionDTO = {
  id?: number;
  name: string;
  description: string;
  roleId: number;
};
